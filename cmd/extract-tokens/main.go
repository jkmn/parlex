package main

import (
	"bytes"
	"flag"
	"go/format"
	"log"
	"os"
	"text/template"

	"gitlab.com/jkmn/parlex/pkg/grammar"

	"gitlab.com/jkmn/errors"
)

var (
	pkg = flag.String("pkg", "", "output package name")
	out = flag.String("o", "", "output file, omit for stdout")
)

func main() {

	flag.Parse()

	if *pkg == "" {
		flag.Usage()
		log.Fatal("output package is required")
	}

	path := flag.Arg(0)
	if path == "" {
		log.Fatal("no path provided")
	}

	g, err := grammar.ParseFile(path)
	if err != nil {
		log.Fatal(err)
	}

	data := make(map[string]interface{})
	terminals := make([]string, 0)
	for _, t := range g.IdentifierNames() {
		if !g.IsTerminal(t) {
			continue
		}
		terminals = append(terminals, t)
	}

	data["Tokens"] = terminals
	data["Package"] = *pkg

	t := template.New("")
	t, err = t.Parse(tmpl)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	buf := bytes.NewBuffer(nil)

	err = t.Execute(buf, data)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	b, err := format.Source(buf.Bytes())
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	w := os.Stdout
	if *out != "" {
		f, err := os.Create(*out)
		if err != nil {
			log.Fatal(errors.Stack(err))
		}
		defer f.Close()
		w = f
	}

	_, err = w.Write(b)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
}

var tmpl = `package {{.Package}}

type Token int

const ({{range $index, $element := .Tokens}}
	{{if eq $index 0}}{{.}} Token = iota{{else}}{{.}}{{end}}{{end}}
	ERR
)

var tokenStrings map[Token]string = map[Token]string{ {{range .Tokens}}
	{{.}}: "{{.}}",{{end}}
}

func (t Token) String() string{
	s, ok := tokenStrings[t]
	if ok {
		return s
	}
	return "unknown"
}
`
