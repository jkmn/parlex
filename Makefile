BINARIES=\
	tokens
SETUP=setup/local
FMT=
include default.mk

setup/local:
	go install golang.org/x/tools/cmd/goyacc@latest

tokens:
	go build ./cmd/extract-tokens


.PHONY: \
	$(BINARIES)