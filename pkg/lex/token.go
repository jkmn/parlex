package lex

type Token interface {
	String() string
}

type internalToken rune

func (i internalToken) String() string {
	m := map[internalToken]string{
		ERR: "ERR",
	}

	return m[i]
}

const (
	ERR internalToken = -2
)
