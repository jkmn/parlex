package lex

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

const (
	EOF rune = -1
)

type Lexer struct {
	Name  string
	Input string
	Start int // start position of the current token.
	Pos   int // working position
	Width int // length of the last rune
	Items chan Item
}

func New(name, input string, init StateFn) (*Lexer, chan Item) {
	l := &Lexer{
		Name:  name,
		Input: input,
		Items: make(chan Item),
	}
	go l.run(init)
	return l, l.Items
}

func (l *Lexer) line() int {
	return strings.Count(l.Input[:l.Start], "\n") + 1
}

func (l *Lexer) col() int {
	i := strings.LastIndex(l.Input[:l.Start], "\n")
	if i == -1 {
		i = 0
	}
	runes := []rune(l.Input[i:l.Start])
	col := len(runes)
	if col == 0 {
		col++
	}
	return col
}

func (l *Lexer) run(init StateFn) {
	for state := init; state != nil; {
		state = state(l)
	}
	close(l.Items)
}

func (l *Lexer) Emit(t Token) {
	l.Items <- Item{
		Token: t,
		Value: l.Input[l.Start:l.Pos],
		Pos:   l.Pos,
		Line:  l.line(),
		Col:   l.col(),
	}
	l.Start = l.Pos
}

func (l *Lexer) Errorf(format string, args ...interface{}) StateFn {
	l.Items <- Item{
		Token: ERR,
		Value: fmt.Sprintf(format, args...),
		Pos:   l.Pos,
		Line:  l.line(),
		Col:   l.col(),
	}
	return nil
}

func (l *Lexer) Next() (r rune) {
	if l.Pos >= len(l.Input) {
		l.Width = 0
		return EOF
	}
	r, l.Width = utf8.DecodeRuneInString(l.Input[l.Pos:])
	l.Pos += l.Width
	return r
}

func (l *Lexer) Peek() rune {
	r := l.Next()
	l.Backup()
	return r
}

func (l *Lexer) Ignore() {
	l.Start = l.Pos
}

func (l *Lexer) Backup() {
	l.Pos -= l.Width
}

func (l *Lexer) Accept(valid string) bool {
	if strings.IndexRune(valid, l.Next()) >= 0 {
		return true
	}
	l.Backup()
	return false
}

func (l *Lexer) AcceptRun(valid string) {
	for strings.IndexRune(valid, l.Next()) >= 0 {
	}
	l.Backup()
}

func (l *Lexer) NextBegins(s string) bool {
	return strings.HasPrefix(l.Input[l.Start:], s)
}
