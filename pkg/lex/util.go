package lex

import "unicode"

func IsNumeric(r rune) bool {
	return unicode.IsDigit(r)
}

func IsAlphaNumeric(r rune) bool {
	return unicode.IsLetter(r) || unicode.IsDigit(r)
}

func IsAlpha(r rune) bool {
	return unicode.IsLetter(r)
}

func IsSpace(r rune) bool {
	return unicode.IsSpace(r)
}
