package lex

import "fmt"

type Item struct {
	Token Token
	Value string
	Pos   int
	Line  int
	Col   int
}

func (i Item) String() string {
	return fmt.Sprintf("[%d:%d]\t%s\t%s", i.Line, i.Col, i.Token, i.Value)
}
