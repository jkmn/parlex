package grammar

import (
	"fmt"
	"log"
	"os"
	"sort"
	"text/tabwriter"
)

type Identifier struct {
	Name     string
	Type     string
	Terminal bool
	Major    int
	Minor    int
}

type IdentifierSet struct {
	set map[string]Identifier
}

type IdentifiersByMajorMinor []Identifier

func (s IdentifiersByMajorMinor) Less(i, j int) bool {
	a := s[i]
	b := s[j]

	if a.Major == b.Major {
		return a.Minor < b.Minor
	}

	return a.Major < b.Major
}

func (s IdentifiersByMajorMinor) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s IdentifiersByMajorMinor) Len() int {
	return len(s)
}

func (is *IdentifierSet) Slice() []Identifier {
	out := make([]Identifier, 0)
	for _, i := range is.set {
		out = append(out, i)
	}

	sort.Sort(IdentifiersByMajorMinor(out))

	return out
}

func (is *IdentifierSet) Incorporate(i Identifier) {
	if is.set == nil {
		is.set = map[string]Identifier{}
	}
	_, ok := is.set[i.Name]
	if !ok {
		is.set[i.Name] = i
		return
	}

	is.UpdateType(i.Name, i.Type)
}

func (is *IdentifierSet) Contains(name string) bool {
	_, ok := is.set[name]
	return ok
}

func (is *IdentifierSet) UpdateType(name, typ string) {
	existing, ok := is.set[name]
	if !ok {
		log.Fatalf("cannot update identifier [%s]; identifier not present", name)
	}

	if existing.Type != "" {
		log.Fatalf("declared identifier [%s] already has a type [%s]", name, existing.Type)
	}

	existing.Type = typ
	is.set[name] = existing
}

func (is *IdentifierSet) Combine(it *IdentifierSet) *IdentifierSet {
	out := &IdentifierSet{}
	ids := is.Slice()
	for _, id := range ids {
		out.Incorporate(id)
	}
	ids = it.Slice()
	for _, id := range ids {
		out.Incorporate(id)
	}

	return out
}

func (is *IdentifierSet) Log() {
	tw := tabwriter.NewWriter(os.Stderr, 0, 2, 1, ' ', 0)
	for _, t := range is.Slice() {
		_, _ = fmt.Fprintf(tw, "[%d,%d]\t%s\t%s\t%t\n", t.Major, t.Minor, t.Name, t.Type, t.Terminal)
	}
	_ = tw.Flush()
}
