package grammar

type Rule struct {
	Production  string
	Code        string `json:"-"`
	Ingredients []string
	start       bool
}

func (r *Rule) HasIngredient(ident string) bool {
	for _, i := range r.Ingredients {
		if i == ident {
			return true
		}
	}
	return false
}

func (r *Rule) startsWith(names []string) bool {
	if len(r.Ingredients) < len(names) {
		return false
	}

	for i, name := range names {
		if r.Ingredients[i] != name {
			return false
		}
	}

	return true
}
