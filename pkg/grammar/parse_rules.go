package grammar

import (
	"strings"

	"gitlab.com/jkmn/errors"
)

func parseRules(b []byte) ([]*Rule, error) {

	// We assume the incoming block begins and ends with %%. Let's just trim
	// those now.
	b = b[2 : len(b)-2]

	/*
		Trying to separate the rules based on semicolons does not work, as the
		code segments may include semicolons. As a result, we need to parse the
		whole block sequentially.
	*/

	rules := make([]*Rule, 0)
	for len(b) > 0 {
		var err error
		b, err = discardCommentsWhitespace(b)
		if err != nil {
			return nil, errors.Stack(err)
		}

		if len(b) == 0 {
			break
		}

		var newRules []*Rule
		b, newRules, err = captureRules(b)
		if err != nil {
			return nil, errors.Stack(err)
		}

		rules = append(rules, newRules...)
	}

	return rules, nil
}

func captureRules(b []byte) ([]byte, []*Rule, error) {
	/*
		A rule will contain an identifier followed by a semicolon.
		After that identifier, the rule will contain one or more recipes.
	*/
	content, err := captureSection(b, "", ":")
	if err != nil {
		return nil, nil, errors.Stack(err)
	}
	b = b[len(content):]

	name := string(content)
	name = strings.TrimSuffix(name, ":")
	name = strings.TrimSpace(name)

	b, err = discardCommentsWhitespace(b)
	if err != nil {
		return nil, nil, errors.Stack(err)
	}

	rules := make([]*Rule, 0)

	// A rule can have a code section right after the declaration.
	if b[0] == '{' {
		code := captureCode(b)
		b = b[len(code):]

		b, err = discardCommentsWhitespace(b)
		if err != nil {
			return nil, nil, errors.Stack(err)
		}

		if b[0] == '|' {
			b = b[1:]
		}

		rule := &Rule{
			Production: name,
			Code:       code,
		}

		rules = append(rules, rule)
	}

	for {

		b, err = discardCommentsWhitespace(b)
		if err != nil {
			return nil, nil, errors.Stack(err)
		}

		rule := &Rule{
			Production: name,
		}

		// The recipe begins with a list of ingredients, all on one line.
		content, err := captureSection(b, "", "\n")
		if err != nil {
			return nil, nil, errors.Stack(err)
		}
		b = b[len(content):]

		rule.Ingredients = captureIngredients(content)

		b, err = discardCommentsWhitespace(b)
		if err != nil {
			return nil, nil, errors.Stack(err)
		}

		/*
			The next character will either be a curly brace, a bar, or a
			semicolon.

			If the next character is a curly brace, then this recipe has a code
			section. We need to capture this now.

			If the next character is a bar, then there is another recipe. We
			need to consume this bar and let the loop continue.

			If the next character is a semicolon, there are no more recipes.
			Break out of the loop.
		*/

		if b[0] == '{' {
			code := captureCode(b)
			b = b[len(code):]
			rule.Code = code

			b, err = discardCommentsWhitespace(b)
			if err != nil {
				return nil, nil, errors.Stack(err)
			}
		}

		rules = append(rules, rule)

		if b[0] == ';' {
			b = b[1:]
			break
		}

		if b[0] == '|' {
			b = b[1:]
			continue
		}
	}

	return b, rules, nil
}

func captureIngredients(content []byte) []string {

	out := make([]string, 0)

	s := string(content)
	for {
		bStart := strings.Index(s, "/*")
		if bStart == -1 {
			break
		}
		bEnd := strings.Index(s, "*/")
		bEnd += 2
		s = s[:bStart] + s[bEnd:]
	}

	ingredients := strings.Split(s, " ")
	for _, i := range ingredients {
		if i != "" {
			// i = strings.TrimSuffix(i, "\n")
			i = strings.TrimSpace(i)
			out = append(out, i)
		}
	}

	return out
}

func captureCode(b []byte) string {
	// We assume that we've hit a code section, which is surrounded by curly
	// braces. Let's count the curly braces, waiting until we have an even
	// number of openings and closings.

	count := 0
	i := 0
	var quoter *byte
	for {
		switch b[i] {
		case '`', '"', '\'':
			if quoter != nil && *quoter == b[i] {
				quoter = nil
			} else if quoter == nil {
				quoter = &b[i]
			}
		case '{':
			if quoter == nil {
				count++
			}
		case '}':
			if quoter == nil {
				count--
			}
		}
		i++

		if count == 0 {
			break
		}
	}

	return string(b[0:i])
}
