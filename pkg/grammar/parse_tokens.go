package grammar

import (
	"strings"
)

func parseTokens(raw []Segment) (*IdentifierSet, error) {
	out := &IdentifierSet{}

	for _, r := range raw {
		s := r.Content()
		s = simplifySpaces(s)
		s = strings.TrimSpace(s)

		typ, names := extractTokenTokens(s)
		for minor, name := range names {
			token := Identifier{
				Name:     name,
				Type:     typ,
				Major:    r.major,
				Minor:    minor,
				Terminal: true,
			}
			out.Incorporate(token)
		}
	}
	return out, nil
}

func extractTokenTokens(s string) (string, []string) {
	s = strings.TrimPrefix(s, "%token")
	s = strings.TrimSpace(s)

	chunks := strings.Split(s, " ")

	// determine the type, if applicable.
	c0 := chunks[0]
	typ := ""
	if strings.HasPrefix(c0, "<") && strings.HasSuffix(c0, ">") {
		typ = strings.Trim(c0, "<>")
		chunks = chunks[1:]
	}

	// get the tokens
	names := chunks
	return typ, names
}

func simplifySpaces(s string) string {
	s = strings.ReplaceAll(s, "\t", " ")
	s = strings.TrimSpace(s)
	prev := len(s)
	post := -1
	for prev != post {
		prev = len(s)
		s = strings.ReplaceAll(s, "  ", " ")
		post = len(s)
	}

	return s
}
