package grammar

import "sort"

type Grammar struct {
	rules                  []*Rule
	identifiers            *IdentifierSet
	cacheFirstTerminals    map[string][]string
	cacheFirstNonTerminals map[string][]string
}

func (g *Grammar) Start() *Rule {
	if len(g.rules) == 0 {
		return nil
	}

	for _, rule := range g.rules {
		if rule.start {
			return rule
		}
	}

	return g.rules[0]
}

func (g *Grammar) IsTerminal(name string) bool {
	ident := g.identifiers.set[name]
	return ident.Terminal
}

func (g *Grammar) RulesByProduction(name string) []*Rule {
	out := make([]*Rule, 0)
	for _, rule := range g.rules {
		if rule.Production == name {
			out = append(out, rule)
		}
	}
	return out
}

func (g *Grammar) RulesThatStartWith(names []string) []*Rule {
	out := make([]*Rule, 0)
	for _, rule := range g.rules {
		if rule.startsWith(names) {
			out = append(out, rule)
		}
	}
	return out
}

func (g *Grammar) FirstProductions(ident string) (terminals, nonTerminals []string) {

	if g.cacheFirstTerminals == nil {
		g.cacheFirstTerminals = map[string][]string{}
	}
	if g.cacheFirstNonTerminals == nil {
		g.cacheFirstNonTerminals = map[string][]string{}
	}

	if g.cacheFirstTerminals[ident] != nil && g.cacheFirstNonTerminals != nil {
		terminals = g.cacheFirstTerminals[ident]
		nonTerminals = g.cacheFirstNonTerminals[ident]
		return
	}

	terminals, nonTerminals = g.firstProductionsIdent(ident, []string{})
	terminals = sRemove(terminals, "")

	terminals = sDedup(terminals)
	nonTerminals = sDedup(nonTerminals)

	sort.Strings(terminals)
	sort.Strings(nonTerminals)

	g.cacheFirstTerminals[ident] = terminals
	g.cacheFirstNonTerminals[ident] = nonTerminals

	return
}

func (g *Grammar) firstProductionsIdent(ident string, parents []string) (terminals, nonTerminals []string) {

	nonTerminals = make([]string, 0)
	terminals = make([]string, 0)

	if sContains(parents, ident) {
		nonTerminals = append(nonTerminals, ident)
		return
	}

	if g.IsTerminal(ident) {
		terminals = append(terminals, ident)
		return
	}

	nonTerminals = append(nonTerminals, ident)

	rules := g.RulesByProduction(ident)

	for _, r := range rules {
		newTerminals, newNonTerminals := g.firstProductionsRule(r, parents)
		terminals = append(terminals, newTerminals...)
		nonTerminals = append(nonTerminals, newNonTerminals...)
	}

	terminals = sDedup(terminals)
	nonTerminals = sDedup(nonTerminals)
	sort.Strings(terminals)
	sort.Strings(nonTerminals)

	return
}

func (g *Grammar) firstProductionsRule(rule *Rule, parents []string) (terminals, nonTerminals []string) {

	nonTerminals = make([]string, 0)
	terminals = make([]string, 0)

	if len(rule.Ingredients) == 0 {
		terminals = append(terminals, "")
		return
	}

	for _, ing := range rule.Ingredients {
		// try to get any new tokens.
		newTerminals, newNonTerminals := g.firstProductionsIdent(ing, append(parents, rule.Production))
		terminals = append(terminals, newTerminals...)
		nonTerminals = append(nonTerminals, newNonTerminals...)

		if !sContains(newTerminals, "") {
			terminals = sRemove(terminals, "")
			break
		}
	}

	terminals = sDedup(terminals)
	nonTerminals = sDedup(nonTerminals)
	sort.Strings(terminals)
	sort.Strings(nonTerminals)

	return
}

func (g *Grammar) Rules() []*Rule {
	return g.rules
}

func (g *Grammar) IdentifierNames() []string {
	out := make([]string, 0)
	for _, id := range g.identifiers.Slice() {
		out = append(out, id.Name)
	}
	return out
}

func sContains(a []string, b string) bool {
	for _, s := range a {
		if s == b {
			return true
		}
	}
	return false
}

func sDedup(a []string) []string {
	m := make(map[string]struct{})
	out := make([]string, 0)

	for _, s := range a {

		_, ok := m[s]
		if ok {
			continue
		}

		out = append(out, s)
		m[s] = struct{}{}
	}

	return out
}

func sRemove(a []string, b string) []string {
	out := make([]string, 0)

	for _, s := range a {
		if s == b {
			continue
		}

		out = append(out, s)
	}

	return out

}
