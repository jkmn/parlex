package grammar

import (
	"io/ioutil"

	"gitlab.com/jkmn/errors"
)

func ParseFile(path string) (*Grammar, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.Stack(err)
	}

	ms, err := captureMainSections(b)
	if err != nil {
		return nil, errors.Stack(err)
	}

	types, err := parseTypes(ms.Types)
	if err != nil {
		return nil, errors.Stack(err)
	}

	tokens, err := parseTokens(ms.Tokens)
	if err != nil {
		return nil, errors.Stack(err)
	}

	// The order is important here.
	// It's possible to declare a terminal's type with a '%type' or a '%token'
	// declaration. When these identifiers are combined, any type derived from a
	// '%type' declaration will apply to any existing terminals, effectively
	// merging the declarations when appropriate.
	combined := tokens.Combine(types)
	// combined.Log()

	rules, err := parseRules(ms.Rule)
	if err != nil {
		return nil, errors.Stack(err)
	}

	for i, r := range rules {
		if combined.Contains(r.Production) {
			continue
		}
		id := Identifier{
			Name:  r.Production,
			Major: 99999,
			Minor: i,
		}
		combined.Incorporate(id)
	}

	gram := &Grammar{
		rules:       rules,
		identifiers: combined,
	}

	return gram, nil
}
