package grammar

import (
	"errors"
	"strings"
)

var (
	errNoTypeFound error = errors.New("type expected, none found")
)

func parseTypes(raw []Segment) (*IdentifierSet, error) {

	var (
		err error
		out = &IdentifierSet{} // map[token]type
	)

	lastType := ""
	for _, r := range raw {
		s := r.Content()
		s = strings.TrimSpace(s)
		s = simplifySpaces(s)

		var (
			typ   string
			names []string
		)
		if strings.HasPrefix(s, "%type") {
			typ, names, err = extractTypeTokens(s)
			if err != nil {
				return nil, err
			}
			lastType = typ
		} else {
			typ = lastType
			names = strings.Split(s, " ")
		}

		for minor, name := range names {
			token := Identifier{
				Name:  name,
				Type:  typ,
				Major: r.major,
				Minor: minor,
			}
			out.Incorporate(token)
		}
	}

	return out, nil
}

func extractTypeTokens(s string) (string, []string, error) {
	s = strings.TrimPrefix(s, "%type")
	s = strings.TrimSpace(s)

	chunks := strings.Split(s, " ")

	// determine the type
	c0 := chunks[0]
	if !strings.HasPrefix(c0, "<") {
		return "", nil, errNoTypeFound
	}
	if !strings.HasSuffix(c0, ">") {
		return "", nil, errNoTypeFound
	}

	typ := strings.Trim(c0, "<>")

	// get the tokens
	names := make([]string, 0)
	chunks = chunks[1:]
	for _, c := range chunks {
		names = append(names, c)
	}

	return typ, names, nil
}
