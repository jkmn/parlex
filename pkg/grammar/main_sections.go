package grammar

import (
	"bytes"
	"errors"
	"fmt"
)

type MainSections struct {
	Comments []Segment
	Source   []byte
	Union    []byte
	Rule     []byte
	Tokens   []Segment
	Types    []Segment
}

var (
	errNoCapture     = errors.New("nothing captured")
	errUnexpectedEOF = errors.New("unexpected EOF")
)

const (
	cType  string = "type"
	cToken string = "token"
)

type Segment struct {
	content []byte
	major   int
}

func (s Segment) Content() string {
	return string(s.content)
}

func captureMainSections(b []byte) (*MainSections, error) {

	major := 1

	sections := &MainSections{
		Comments: make([]Segment, 0),
		Tokens:   make([]Segment, 0),
		Types:    make([]Segment, 0),
	}

	var lastCapture string

	for len(b) > 0 {

		content, err := captureWhiteSpace(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			b = b[len(content):]
			continue
		}

		content, err = captureLineComment(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			segment := Segment{
				content: content,
				major:   major,
			}
			sections.Comments = append(sections.Comments, segment)
			b = b[len(content):]
			continue
		}

		content, err = captureBlockComment(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			segment := Segment{
				content: content,
				major:   major,
			}
			sections.Comments = append(sections.Comments, segment)
			b = b[len(content):]
			continue
		}

		content, err = captureSourceSection(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			sections.Source = content
			b = b[len(content):]
			continue
		}

		content, err = captureUnionSection(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			sections.Union = content
			b = b[len(content):]
			continue
		}

		content, err = captureToken(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			major++
			segment := Segment{
				content: content,
				major:   major,
			}
			sections.Tokens = append(sections.Tokens, segment)
			b = b[len(content):]
			lastCapture = cToken
			continue
		}

		content, err = captureType(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			major++
			segment := Segment{
				content: content,
				major:   major,
			}
			sections.Types = append(sections.Types, segment)
			b = b[len(content):]
			lastCapture = cType
			continue
		}

		content, err = captureRuleSection(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			sections.Rule = content
			b = b[len(content):]
			continue
		}

		// this should always be last.
		content, err = captureTrailings(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			segment := Segment{
				content: content,
				major:   major,
			}
			if lastCapture == cToken {
				major++
				sections.Tokens = append(sections.Tokens, segment)
			} else if lastCapture == cType {
				major++
				sections.Types = append(sections.Types, segment)
			}
			b = b[len(content):]
			continue
		}
	}

	return sections, nil
}

func (ms MainSections) String() string {

	buf := bytes.NewBuffer(nil)

	fmt.Fprint(buf, "==UNION SECTION==\n", string(ms.Union))
	fmt.Fprint(buf, "==RULES SECTION==\n", string(ms.Rule))
	fmt.Fprint(buf, "==SOURCE SECTION==\n", string(ms.Source))
	fmt.Fprint(buf, "==COMMENTS==")
	for _, s := range ms.Comments {
		fmt.Fprint(buf, s.Content())
	}
	fmt.Fprint(buf, "==TOKENS==")
	for _, s := range ms.Tokens {
		fmt.Fprint(buf, s.Content())
	}
	fmt.Fprint(buf, "==TYPES==")
	for _, s := range ms.Types {
		fmt.Fprint(buf, s.Content())
	}

	return buf.String()
}

func captureTrailings(b []byte) ([]byte, error) {
	return captureSection(b, "", "\n")
}

func captureToken(b []byte) ([]byte, error) {
	return captureSection(b, "%token", "\n")
}

func captureType(b []byte) ([]byte, error) {
	return captureSection(b, "%type", "\n")
}

func captureBlockComment(b []byte) ([]byte, error) {
	return captureSection(b, "/*", "*/")
}

func captureWhiteSpace(b []byte) ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	for len(b) > 0 {
		captured := false
		switch b[0] {
		case ' ', '\n', '\t':
			buf.WriteByte(b[0])
			b = b[1:]
			captured = true
		default:
			captured = false
		}
		if !captured {
			break
		}
	}

	if buf.Len() == 0 {
		return nil, errNoCapture
	}

	return buf.Bytes(), nil
}

func captureRuleSection(b []byte) ([]byte, error) {
	return captureSection(b, "%%", "%%")
}

func captureLineComment(b []byte) ([]byte, error) {
	return captureSection(b, "//", "\n")
}

func captureSourceSection(b []byte) ([]byte, error) {
	return captureSection(b, "%{", "%}")
}

func captureUnionSection(b []byte) ([]byte, error) {
	return captureSection(b, "%union{", "}")
}

func captureSection(b []byte, start, end string) ([]byte, error) {
	if len(b) < len(start) || string(b[:len(start)]) != start {
		return nil, errNoCapture
	}

	buf := bytes.NewBuffer(nil)

	// capture the start
	buf.Write(b[:len(start)])
	b = b[len(start):]

	// capture everything until we reach the end.
	for {
		if len(b) < len(end) {
			return nil, errUnexpectedEOF
		}

		if string(b[:len(end)]) == end {
			break
		}

		// we haven't reach the end, and we still have bytes.
		// pop the next byte and continue.
		buf.Write(b[:1])
		b = b[1:]
	}

	// capture the end
	buf.Write(b[:len(end)])

	return buf.Bytes(), nil
}

func discardCommentsWhitespace(b []byte) ([]byte, error) {

	start := 0

	for start != len(b) {
		start = len(b)

		content, err := captureWhiteSpace(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			b = b[len(content):]
			continue
		}

		content, err = captureLineComment(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			b = b[len(content):]
			continue
		}

		content, err = captureBlockComment(b)
		if err != nil && err != errNoCapture {
			return nil, err
		} else if err == nil {
			b = b[len(content):]
			continue
		}
	}

	return b, nil
}
