CGO_ENABLED=0
CI_COMMIT_SHA=$(shell git rev-parse HEAD)
CI_COMMIT_SHORT_SHA=$(shell git rev-parse --short HEAD)
OS=$(shell uname -s)

XARGS=$(shell which xargs)
ifeq ($(OS),Darwin)
  # BSD xargs enforces limitations on replacement size and quantity.
  # Use GNU xargs instead.
  XARGS=$(shell which gxargs)
endif

help: ## show this help
	@grep -h -E '^[\.a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
		| sort \
		| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%s\033[0m:%s\n", $$1, $$2}' \
		| column -s ':' -t

all: $(BINARIES) ## build all binaries

clean: ## delete temporary files and binaries
	rm -f $(BINARIES)
	rm -f *.log
	rm -f coverage.out

coverage: integration ## execute integration tests and show coverage in browser
	go tool cover -html=coverage.out

fmt: $(FMT) ## format sources, including nginx, go, json, yaml, and shell
	@find . -name '*.go' \
		| fgrep -v '/vendor/' \
		| fgrep -v '.y.go' \
		| $(XARGS) -I{} -t goimports -w {}
	@find . -name '*.json' \
		| fgrep -v '/node_modules/' \
		| fgrep -v '/vendor/' \
		| fgrep -v package-lock.json \
		| fgrep -v package.json \
		| $(XARGS) -I{} sh -c "echo {}; jq '.' {} | sponge {}"
	@find . -name '*.yaml' \
		| fgrep -v '/node_modules/' \
		| fgrep -v '/vendor/' \
		| $(XARGS) -I{} -t yamlfmt -w {}
	@find . -name '*.yml' \
		| fgrep -v '/node_modules/' \
		| fgrep -v '/vendor/' \
		| $(XARGS) -I{} -t yamlfmt -w {}
	@find . -name '*.sh' \
		| fgrep -v '/node_modules/' \
		| fgrep -v '/vendor/' \
		| $(XARGS) -I{} -t shfmt -w -i 2 -sr {}

generate:
	go generate ./...

integration: ## execute integration tests
	# execute the tests.
	go test -tags integration -coverprofile=coverage.out -v ./...

lint: ## execute source linters
	golangci-lint run ./...

setup: setup/$(OS) $(SETUP) ## install tooling
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $$(go env GOPATH)/bin v1.58.0
	go install github.com/UltiRequiem/yamlfmt@latest
	go install github.com/vektra/mockery/v2@latest
	go install golang.org/x/tools/cmd/goimports@latest
	go install mvdan.cc/sh/v3/cmd/shfmt@latest

setup/Darwin:
	brew install coreutils

setup/Linux:

test: lint ## execute tests, excluding integration tests
	go test -v -timeout 10s ./...
	go vet -composites=false $$(go list ./... | grep -v /vendor/)

upgrade: ## upgrade source dependencies
	go get -u -t all
	go mod tidy
	go mod vendor
	go mod verify
#	npm update
#	npm outdated

vendor: ## tidy and vendor source dependencies
	go mod tidy
	go mod vendor
	go mod verify


.PHONY: \
	$(BINARIES) \
	$(FMT) \
	$(SETUP) \
	all \
	clean \
	coverage \
	fmt \
	generate \
	help \
	integration \
	lint \
	setup \
	setup/* \
	test \
	upgrade \
	vendor
