module gitlab.com/jkmn/parlex

go 1.22

require gitlab.com/jkmn/errors v0.0.0-20211020041744-46f7d817a344

require github.com/stretchr/testify v1.9.0 // indirect
